
jQuery(document).ready(function($) {
    
    var fileCollection = Array();
    
    var output_wrapp    = $( "#ilsh-result-wrapp" );
    
    var output          = $( "#ilsh-result-table" );
    
    var upload_button   = $( ".ilsh-files" );
    
    var mess_popup      = $( "#ilsh-mess-popup" );
    
    var overlay         = $( "#ilsh-overlay" );
    
    var sortable_gallery= $( "#ilsh-sortable-gallery" );
    
    var post_id         = $( "#ilsh-post-id" ).val();
    
    sortable_gallery.sortable( {
        
        update: function( event, ui ) {
            
            ilsh_save_gallery();
        }
    });   

    sortable_gallery.disableSelection();    
         
    $(document).on( "click", ".ilsh-preview-remove", function() {
       
        if( fileCollection.length > 1 ) {
            
            fileCollection.splice( $( this ).attr( "id" ), 1 );
            
            $( this ).closest( "tr" ).fadeOut( 300, function() { 
                
                $( this ).remove(); 
                
                ilsh_set_buttons_ids(); 
            });
            
        } else {
            
            ilsh_remove_all_preview();
        }

    });
    
    $(document).on( "click", "#ilsh-clear", function() {
       
        ilsh_remove_all_preview();
  
    });
    
    $(document).on( "click", "#ilsh-select", function() {
       
        upload_button.trigger( "click" );
  
    });
    
    function ilsh_remove_all_preview() {
        
        output.find( "tr" ).fadeOut( 300, function() { 
            
            $( this ).remove(); 
        });        
            
        fileCollection.length = 0;
         
        upload_button.attr( "value", "" );
        
        output_wrapp.hide();        
    }
    
    function ilsh_save_gallery() {
        
        var gallery_hidden_input = $( ".ilsh-gallery-hidden-input" );
        
        var ids = Array();
        
        if( gallery_hidden_input.length ) {
            
            gallery_hidden_input.each( function() {
            
                ids.push( $( this ).val() );
            });
        }        
        
        var data = {
            "action"        : "ilsh_gallery_save",
            "ilsh_post_id"  : post_id,
            'ilsh_gallery'  : ids
        };
         
        $.ajax({
            
            type: "POST",
            url: _admin_data.ajax_url,
            data: data,
            dataType: 'json',
            success: function ( response ) {
                
            }
        });
    }
    
    
    window.onload = function () {
        
        if ( window.File && window.FileList && window.FileReader ) {
            
            $(document).on( "change", ".ilsh-files", function( event ) {

                var regex = /(.jpg|.jpeg|.gif|.png|.bmp)$/;
             
                var value = $( this ).val();

                if ( regex.test( value.toLowerCase() ) ) {
                    
                    var files = event.target.files; 
                    
                    for ( var i = 0; i < files.length; i++ ) {
                        
                        var file = files[i];                      
                        
                        fileCollection.push( file );
                        
                        var fileCounter = fileCollection.length - 1; 
                        
                        var image_size = Math.round( file.size * 0.001 );
                      
                        if ( file.size < 2097152 ) { 
                            
                            var html = "";
                            
                            html = "<tr>";
                            
                            html += "<td></td>";
                            
                            html += "<td ><span class='ilsh-preview-name'>" + file.name + "</span></td>";
                                
                            html += "<td><span class='ilsh-preview-size'>" + image_size + " kB</span></td>";
                            
                            html += "<td><button data-id='" + fileCounter + "' type='button' class='button button-primary ilsh-preview-add'>";
                                
                            html += _admin_data.ilsh_js.button_add +"</button></td>";
                                
                            html += "<td><button id='" + fileCounter + "' type='button' class='button button-primary ilsh-preview-remove'>";
                                
                            html += _admin_data.ilsh_js.button_remove +"</button></td>";
                                
                            html += "</tr>";
                            
                            output.prepend( html );

                            var img = document.createElement( "img" );
                            
                            img .width = "70";

                            var td_output = output.find( "tr:eq(0)" ).find( "td:eq(0)" );
               
                            td_output.append( img );
                            
                            var reader = new FileReader();

                            reader.onload = (function ( _img ) {
                                
                                return function ( evt ) {

                                        _img.src = evt.target.result;
                                };
                                
                            }( img ) );
                            
                            fileCounter++;
                            
                            output_wrapp.show();
                            
                            reader.readAsDataURL( file );
                            
                        } else {
                            
                            ilsh_remove_all_preview();
                            
                            alert( _admin_data.ilsh_js.valid_img_size );
                        }                        
                    }
           
                } else {
                    
                    ilsh_remove_all_preview();
                    
                    alert( _admin_data.ilsh_js.valid_img_type );
                }

            }); 
        }
    }
    
    $(document).on( "click", "#ilsh-upload", function( e ) {
        
        e.preventDefault();
        
        var form = new FormData();
        
        form.append( "action", "ilsh_gallery_ajax" );
        
        form.append( "post_id", post_id );
   
        for( var i=0; i < fileCollection.length; i++ ) {
            
            form.append( "ilsh_gallery_preview[]", fileCollection[i] );
        } 
        
        mess_popup.show();
        
        overlay.show();
        
        $.ajax({            
            type: "POST",
            url: _admin_data.ajax_url,
            data: form,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function ( response ) {
                
                if( response.status ) {
                    
                    var last_mess = mess_popup.html();
                    
                    setTimeout( function() { 
                    
                        mess_popup.addClass( response.status ).html( response.message ).fadeIn("fast");                
                        
                        if( response.new_gallery ) {
                           
                            sortable_gallery.html( response.new_gallery );
                        }
                        
                        setTimeout( function() {
                            
                            mess_popup.hide( "slow", function() {
                                
                                mess_popup.html( last_mess ).removeClass( response.status );
                            });
                            
                            overlay.hide( "slow" );

                            ilsh_remove_all_preview();

                        }, 1500 );
                        
                    }, 1000 );    
                }
            }
        });
        
    });
    
    $(document).on( "click", ".ilsh-gallery-remove", function( event ) {
       
        var remove = confirm( _admin_data.ilsh_js.remove_confirm );
        
        if( remove == true ) {
            
            $( this ).parent().fadeOut( 300, function() { 
                
                $( this ).remove();

                ilsh_save_gallery();
            });
        }
    });
   
    $(window).bind('tb_unload', function () {
        
       $( '.ilsh-update-attachment-mess' ).hide();
    });
      
    $(document).on( "click", ".ilsh-update-attachment-data", function( event ) {
        
        event.preventDefault();
        
        var parrent = $( this ).closest( '.ilsh-update-attachment-table' );
        
        var data = {
            action  : 'ilsh_update_attachment_data_ajax',
            id      : parrent.find( '.attachment-data-id' ).val(),
            title   : parrent.find( '.attachment-data-title' ).val(),
            alt     : parrent.find( '.attachment-data-alt' ).val(),
            text    : parrent.find( '.attachment-data-text' ).val(),
        };
       
        $.ajax({            
            type: "POST",
            url: _admin_data.ajax_url,
            data: data,            
            dataType: "json",
            success: function ( response ) {
                if( response ) {
                    parrent.find( '.ilsh-update-attachment-mess' ).find( 'td' ).text( response.message );
                    parrent.find( '.ilsh-update-attachment-mess' ).fadeIn();
                    setTimeout( function() { parrent.find( '.ilsh-update-attachment-mess' ).fadeOut(); }, 2000 );
                }
            }                
        });
    });    
    
    function ilsh_set_buttons_ids(){
        
        $( output.find( "tr" ).get().reverse() ).each( function( index ) { 
            
            var add_button      = $( this ).find( ".ilsh-preview-add" );
            
            var remove_button   = $( this ).find( ".ilsh-preview-remove" );
            
            add_button.attr( "data-id", index );
            
            remove_button.attr( "id", index );
        });
    }
    
    $(document).on( "click", ".ilsh-preview-add", function( e ) {
        
        var id = $( this ).attr( "data-id" ); 
        
        e.preventDefault();
        
        var form = new FormData();
        
        form.append( "action", "ilsh_gallery_ajax" );
        
        form.append( "post_id", post_id );
        
        form.append( "ilsh_gallery_preview[]", fileCollection[id] );
        
        mess_popup.show();
        
        overlay.show();
         
        fileCollection.splice( id, 1 ); 
            
        $( this ).closest( "tr" ).fadeOut( 300, function() { 

            $( this ).remove(); 
            
            ilsh_set_buttons_ids();            
        });        

        if( fileCollection.length < 1 ) {
            
            ilsh_remove_all_preview();             
        }
        
        $.ajax({            
            type: "POST",
            url: _admin_data.ajax_url,
            data: form,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function ( response ) {
                
                if( response.status ) {
                                        
                    var last_mess = mess_popup.html();
                    
                    setTimeout( function() { 
                    
                        mess_popup.addClass( response.status ).html( response.message ).fadeIn("fast");                
                        
                        if( response.new_gallery ) {
                            
                            sortable_gallery.html( response.new_gallery );
                        }
                        
                        setTimeout( function() {
                            
                            mess_popup.hide( "slow", function() {
                                
                                mess_popup.html( last_mess ).removeClass( response.status );
                            });
                            
                            overlay.hide( "slow" );                            

                        }, 1000 );
                        
                    }, 1000 );    
                }
            }
        });
        
    });

    
});