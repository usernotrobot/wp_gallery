<?php

class ilsh_config {
    
    public function __construct() {
        
        $this->c = 1;
        
        $this->enabled_fancybox = get_option( 'ilsh_enabled_fancybox');
        
        $this->ilsh_js       = array(
            'remove_confirm'    => __( 'Remove this image from Gallery?', 'ilsh' ), 
            'valid_img_size'    => __( 'Image Size is too big. Maximum size is 2MB.', 'ilsh' ), 
            'valid_img_type'    => __( 'Please upload a valid image file.', 'ilsh' ), 
            'button_remove'     => __( 'remove', 'ilsh' ),  
            'button_add'        => __( 'upload', 'ilsh' )
        );
        
        $this->setting_page = array(
            'page-title'        => __( 'Gallery Settings', 'ilsh' ), 
            'menu-title'        => __( 'Gallery', 'ilsh' ), 
            'menu-slug'         => __( 'ilsh_gallery', 'ilsh' ), 
            'page-top-info'     => __( 'Include post types:', 'ilsh' ),
            'button-text'       => __( 'Save changes', 'ilsh' ),
            'page-setting-info' => __( 
                                        'Shortcode is used to add a gallery : <strong>[ilsh_show_gallery]</strong>.'
                                        . '<br>If you add <strong>‘post_id= id’</strong> parameter to the shortcode '
                                        . 'of any gallery post, you can show several galleries.'
                                        . '<br>If you add <strong>‘thumb_size=’</strong> parameter to the shortcode, for example,'
                                        . ' <strong>‘thumbnail’</strong> or any other registered image size in <strong>WordPress</strong>,'
                                        . ' then size of the gallery images will change to that size parameter.'
                                        . '<br>Developers can use <strong>‘ilsh_get_gallery_attachments( $post_id )’</strong> '
                                        . 'function to obtain the object of all images in the gallery, where'
                                        . '   <strong>‘$post_id’</strong> is <strong>ID</strong> of the post that has a gallery. If this parameter'
                                        . ' is not specified, program will use ID of the post that shows the gallery.',
                                        'ilsh' ),
            'fancy-text'        => __( 'Enabled FancyBox on frontend galleries?', 'ilsh' ),
        );
        
        $this->gallery_page = array(
            'gallery-title'     => __( 'Gallery', 'ilsh' ),          
            'gallery-slug'      => __( 'ilsh_post_type_gallery', 'ilsh' ), 
            'page-top-info'     => __( 'For creating gallery, upload files and sort them:', 'ilsh' ),
            'page-bottom-info'  => __( 'You can preview the images before they load', 'ilsh' ),
            'upload-label'      => __( 'Select multiple files.', 'ilsh' ),
            'preview-text'      => __( 'Live Preview', 'ilsh' ),
            'clear-button-text' => __( 'Clear all', 'ilsh' ),
            'button-select'     => __( 'Select files', 'ilsh' ),
            'button-upload'     => __( 'Upload all', 'ilsh' ),
            'start-upload-mess' => __( 'Upload to Gallery. Do not press any buttons or refresh your browser.', 'ilsh' ),
            'error-upload-mess' => __( 'Failed upload to Gallery!', 'ilsh' ),
            'success-upload-mess' => __( 'Success upload to Gallery.', 'ilsh' ),
            'gallery-edit-link-title' => __( 'Update slide text info', 'ilsh' )             
        );
        
    }
}


