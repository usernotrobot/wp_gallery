<?php


class ilsh_create_gallery extends ilsh_gallery {
    
    public function __construct() {

        parent::__construct();
        
        add_action( 'add_meta_boxes', array( $this, 'ilsh_add' ) );
        
        add_action( 'wp_ajax_ilsh_gallery_ajax', array( $this, 'ilsh_gallery_ajax' ) );
        
        add_action( 'wp_ajax_nopriv_ilsh_gallery_ajax', array( $this, 'ilsh_gallery_ajax' ) );
        
        add_action( 'wp_ajax_ilsh_gallery_save', array( $this, 'ilsh_save' ) );
        
        add_action( 'wp_ajax_nopriv_ilsh_gallery_save', array( $this, 'ilsh_save' ) );
        
        add_action( 'save_post', array( $this, 'ilsh_save' ) );
        
        add_action( 'wp_ajax_ilsh_update_attachment_data_ajax', array( $this, 'ilsh_update_attachment_data_ajax' ) );
        
        add_action( 'wp_ajax_nopriv_ilsh_update_attachment_data_ajax', array( $this, 'ilsh_update_attachment_data_ajax' ) );

    }    
    
    public function ilsh_add() {
        
        $checked_post_types = $this->ilsh_get_checked_post_types();
        
        if( $checked_post_types ) {
            
            foreach( $checked_post_types as $post_type ) {
            
                add_meta_box(
                    $this->gallery_page['gallery-slug'],  
                    $this->gallery_page['gallery-title'],   
                    array( $this, 'ilsh_show' ),  
                    $post_type, 
                    'normal', 
                    'high'
                ); 
            } 
        }
    }
    
    public function ilsh_show() {
    
        global $post;
                
        $this->ilsh_get_gallery( $post );
        
        $this->ilsh_get_upload_form( $post );
    }
    
    public function ilsh_get_edit_slide_form( $attachment ) {

        $data = get_post_meta( $attachment->ID, 'ilsh_attachment_data', true );
        
        ?>
            <div id="edit_slide_form<?php echo $attachment->ID?>" style="display:none;">
                <table class="ilsh-update-attachment-table">
                    <tr class="ilsh-update-attachment-mess" style="display:none;">                        
                        <td colspan="2">
                            
                        </td>                        
                    </tr>
                    <tr>
                        <td>
                            <lable>Title</lable>
                        </td>
                        <td>
                            <input type="text" class="attachment-data-title" value="<?php echo isset( $data['title'] ) ? $data['title'] : '';?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <lable>Alt</lable>
                        </td>
                        <td>
                            <input type="text" class="attachment-data-alt" value="<?php echo isset( $data['alt'] ) ? $data['alt'] : '';?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <lable>Text</lable>
                        </td>
                        <td>
                            <textarea class="attachment-data-text"><?php echo isset( $data['text'] ) ? $data['text'] : '';?></textarea>
                        </td>
                    </tr>
                    <tr>                        
                        <td colspan="2">
                            <input type="hidden" class="attachment-data-id" value="<?php echo $attachment->ID;?>">
                            <button type="button" class="button button-primary ilsh-update-attachment-data">update</button>
                        </td>                        
                    </tr>
                </table>
            </div>
        <?php
    }
    
    public function ilsh_get_upload_form( $post ) {
         
        ?>

        <div class="ilsh-upload-block"> 

            <label for="ilsh-files">
                <?php echo $this->gallery_page['upload-label'];?>
            </label>            
            
            <input  class="ilsh-files" type="file" name="ilsh_files[]" multiple/>
            
            <p>
                <button type="button" class='button button-primary' id="ilsh-select">
                       <?php echo $this->gallery_page['button-select'];?>
                </button>
            </p>
            
            <p id="ilsh-live-preview">
                <b>
                    <?php echo $this->gallery_page['preview-text'];?>
                </b>
            </p>
            
            <div id="ilsh-result-wrapp">
                
                <p>
                    <button type="button" class='button button-primary' id="ilsh-clear">
                        <?php echo $this->gallery_page['clear-button-text'];?>
                    </button>
                    
                    <button type="button" class='button button-primary' id="ilsh-upload">
                        <?php echo $this->gallery_page['button-upload'];?>
                    </button>
                </p>
                
                <table id="ilsh-result-table">
                    
                </table>
                
            </div>
            
            <p>
                <?php echo $this->gallery_page['page-bottom-info'];?>
            </p>
           
        </div>  

        <input type="hidden" id="ilsh-post-id" value="<?php echo $post->ID;?>">	
        
        <div id="ilsh-overlay" class="ilsh-hide"></div>
        
        <div id="ilsh-mess-popup" class="ilsh-hide">
             <img class="ilsh-ajax-loader" src="<?php echo ILSH_PLUGIN_URL . '/assets/images/ajax-loader.gif';?>">             
             <?php echo $this->gallery_page['start-upload-mess'];?>
        </div>
			
        <?php 
    }
    
    public function ilsh_get_gallery( $post, $thumb_size = null ) {
        
        $include = array();
        
        $attachments = array();
        
        $include = get_post_meta( $post->ID, 'ilsh_gallery', true );
        
        if( $include ) {
            
            $include        = json_decode( $include, true );
            
            $attachments    = $this->ilsh_get_post_attachments( $post->ID, $include );            
        }
       
        ?>
            <div class="ilsh-gallery-block">
                <ul class="ilsh-gallery-ul" id="<?php echo is_admin() ? 'ilsh-sortable-gallery' : '';?>">
                    <?php echo ( $attachments ) ? $this->ilsh_get_gallery_li( $attachments, $thumb_size, $post->ID ) : '';?>
                </ul>
            </div>
        <?php        
    }
    
    public function ilsh_get_gallery_li( $attachments, $thumb_size = null, $post_id = null ) {
        
        ob_start();
        
        $thum_light = '';
        
        foreach( $attachments as $attachment ) {
            
            $class      = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type ); 
            
            $size       = 'thumbnail';
            
            if( $thumb_size ) {
                
                $size = $thumb_size;
            }
            
            $thumb      = wp_get_attachment_image_src( $attachment->ID, $size );
            
            if( ! is_admin() && $this->enabled_fancybox ) {
                
                $thum_light = wp_get_attachment_image_src( $attachment->ID, 'large');
                
                $thum_light = $thum_light[0];
            }
            ?>
                <li class="ilsh-gallery-li <?php echo is_admin() ? 'ilsh-gallery-admin-li ui-state-default' : '';?>" >
                    
                    <?php if( is_admin() ):?>
                    
                        <a href="#TB_inline?width=full&height=full&max-height=300&max-width=300&inlineId=edit_slide_form<?php echo $attachment->ID;?>" class="ilsh-gallery-edit-link thickbox" title="<?php echo $this->gallery_page['gallery-edit-link-title'];?>"><img class="ilsh-gallery-edit" src="<?php echo ILSH_PLUGIN_URL . '/assets/images/edit.png';?>" alt="Edit Slide" title="Edit Slide"></a>
                        
                        <?php $this->ilsh_get_edit_slide_form( $attachment );?>
                        
                        <img class="ilsh-gallery-remove" src="<?php echo ILSH_PLUGIN_URL . '/assets/images/cross.png';?>" alt="Remove Image form Gallery" title="Remove Image form Gallery">

                        <input type="hidden" name="ilsh_gallery[]" class="ilsh-gallery-hidden-input" value="<?php echo $attachment->ID;?>">

                    <?php endif;?>
                    
                    <?php if( ! is_admin() && $this->enabled_fancybox ):?> 
                        <a class="ilsh-fancybox" rel="gallery_<?php echo $post_id;?>" href="<?php echo $thum_light;?>" title="">
                    <?php endif;?>
                            
                            <img id="<?php echo $attachment->ID;?>" class="<?php echo is_admin() ? 'ilsh-stable-size' : '';?> ilsh-gallery-attachment <?php echo $class;?>" src="<?php echo $thumb[0];?>" alt="<?php echo $attachment->post_title;?>" title="<?php echo $attachment->post_title;?>">
                        
                    <?php if( ! is_admin() && $this->enabled_fancybox ):?> 
                        </a>
                    <?php endif;?>                        
                        
                </li>
            <?php
        }
        
        return ob_get_clean();
    }


    public function ilsh_save( $post_id = null ) {

        if( isset( $_POST['action'] ) && $_POST['action'] == 'ilsh_gallery_save' ) {
            
            $post_id = $_POST['ilsh_post_id'];
        }  
        
        if( ! $post_id ) return;
        
        if( isset( $_POST['ilsh_gallery'] ) ) {            
          
            //clear attacments  
            $this->ilsh_remove_unnecessary_images( $post_id, $_POST['ilsh_gallery'] );
            
            //save gallery image order
            update_post_meta( $post_id, 'ilsh_gallery', json_encode( $_POST['ilsh_gallery'] ) );            
            
        } else {
            
            //clear attacments  
            $this->ilsh_remove_unnecessary_images( $post_id );
            
            //delete gallery
            delete_post_meta( $post_id, 'ilsh_gallery' );
        }                
    }
    
    private function ilsh_remove_unnecessary_images( $post_id, $new_gallery = array() ) {
        
        $diff        = array();
        
        $old_gallery = get_post_meta( $post_id, 'ilsh_gallery', true );
                
        if( $old_gallery ) {
            
            $old_gallery = json_decode( $old_gallery, true );
            
            if( $new_gallery ) {

                    $diff = array_diff( $old_gallery, $new_gallery ); 

                if( $diff ) {
                    
                    $this->ilsh_delete_attachments( $diff );
                    
                }
            } else {
                
                $this->ilsh_delete_attachments( $old_gallery );              
            }            
        }
    }
    
    private function ilsh_delete_attachments( $attachments ) {
        
        foreach( $attachments as $attachment_id ) {

            wp_delete_attachment( $attachment_id );
        }
    }


    public function ilsh_gallery_ajax(){

        if( isset( $_POST['action'] ) && $_POST['action'] == 'ilsh_gallery_ajax' ) {
         
            if( isset( $_FILES['ilsh_gallery_preview'] ) ) {
             
                $post_id = $_POST['post_id'];

                $uploads = array();

                $uploads = $this->ilsh_upload_gallery( $_FILES['ilsh_gallery_preview'] );
               
                if( $uploads ) {
                   
                    $attacmets_ids = $this->ilsh_insert_attachments( $post_id, $uploads );
                   
                    if( $attacmets_ids ) {
                       
                        $attacmets_ids = array_reverse( $attacmets_ids );
                       
                        $include = array();
                       
                        $include = get_post_meta( $post_id, 'ilsh_gallery', true );
                        
                        if( $include ) {
                            
                            $include = json_decode( $include, true );
                           
                            $attacmets_ids = array_merge( $attacmets_ids, $include );                           
                        }                        
                        
                        update_post_meta( $post_id, 'ilsh_gallery', json_encode( $attacmets_ids ) );
                        
                        $attachments = $this->ilsh_get_post_attachments( $post_id, $attacmets_ids ); 
           
                        echo json_encode( 
                                array( 
                                    'status' => 'success',
                                    'message' => $this->gallery_page['success-upload-mess'],
                                    'new_gallery' => $this->ilsh_get_gallery_li( $attachments )
                                ) 
                        );
                        
                        die;
                    }                   
                }
               
                echo json_encode( array( 'status' => 'error', 'message' => $this->gallery_page['error-upload-mess'] ) );
                die;
            }
        }
    }
    
    private function ilsh_upload_gallery( $files = null ) {
        
        if( ! $files ) {
            
            return;
        }
        
        if ( ! function_exists( 'wp_handle_upload' ) ) {
            
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }
        
        $data = array();
        
        $files_data = $this->ilsh_prepare_gallery_files_data( $files );
        
        if( $files_data ) {
                                   
            foreach( $files_data as $file ) {
                
                $value = $this->ilsh_upload_file( $file );
                
                if( $value ) {
                    
                    $data[] = $value;
                }                
            }
        }
        
        return $data;
    }
    
    private function ilsh_prepare_gallery_files_data( $files ) {
        
        $files_data = array();
        
        for( $i = 0; $i < count( $files['name'] ); $i++ ){
            
            $files_data[] = array(
                'name'      => $files['name'][$i],
                'type'      => $files['type'][$i],
                'tmp_name'  => $files['tmp_name'][$i],
                'error'     => $files['error'][$i],
                'size'      => $files['size'][$i],
            );
        }
        
        return $files_data;
    }
    
    private function ilsh_upload_file( $file ) {
        
        return  wp_handle_upload( $file, array( 'test_form' => false ) );
    }
    
    private function ilsh_insert_attachments( $post_id, $files ) {
        
        $data = array();
        
        foreach( $files as $key => $file ){
            
            $value = $this->ilsh_insert_attachment( $post_id, $file );  
            
            if( $value ) {
                
                $data[$key] = $value;
            }                                     
        }
        
        return $data;
    }
    
    private function ilsh_insert_attachment( $post_id, $file ) {
        
        $attach_id      = null;
        
        $filename       = $file['file'];
        
        $filetype       = wp_check_filetype( basename( $filename ), null );
        
        $wp_upload_dir  = wp_upload_dir();
            
        $attachment = array(
                'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
        );
            
        $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
        
        if( $attach_id ) {
            
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            
            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );

            wp_update_attachment_metadata( $attach_id, $attach_data );
        }
        
        return $attach_id;
    }
    
    public function ilsh_get_post_attachments( $post_id, $include ) {
        
        return get_posts( array(
                'post_type'         => 'attachment',
                'posts_per_page'    => -1,
                'post_parent'       => $post_id,
                'exclude'           => get_post_thumbnail_id(), 
                'include'           => $include,
                'orderby'           => 'post__in'
        ) );	
    }
    
    public function ilsh_update_attachment_data_ajax() {
    
        if( isset( $_POST['action'] ) && $_POST['action'] == 'ilsh_update_attachment_data_ajax' ) {

            update_post_meta( $_POST['id'], 'ilsh_attachment_data', array(
                'title' => $_POST['title'],
                'alt'   => $_POST['alt'],
                'text'  => $_POST['text'],
            ) );
            
            echo json_encode( array( 'status' => 'success', 'message' => 'Update Success!' ) );
            die;
        }
    }
    
}

new ilsh_create_gallery();

/*
 * To get post gallery attachments on front end
 */
function ilsh_get_gallery_attachments( $post_id = null ) {
    
    if( ! $post_id ) {
        
        $post_id = get_the_ID();
    }
    
    if( ! $post_id ) {
        
        return;
    }
    
    $ilsh_create_gallery = new ilsh_create_gallery();    
    
    $include = array();
        
    $attachments = array();

    $include = get_post_meta( $post_id, 'ilsh_gallery', true );

    if( $include ) {

        $include        = json_decode( $include, true );

        $attachments    = $ilsh_create_gallery->ilsh_get_post_attachments( $post_id, $include );            
    }
    
    return $attachments;
}

