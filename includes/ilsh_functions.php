<?php

class ilsh_functions extends ilsh_config{
    
    public function __construct() {
        
        parent::__construct(); 
    }
    
    public function islh_get_post_types() {
        
        //set default post types
        $post_types = array(
            'post' => 'Posts',
            'page' => 'Pages'
        );        
                
        $args = array(
            'public'   => true,
            '_builtin' => false,
        );

        $output = 'objects'; 
        
        //get all custom post types
        $custom_post_types = get_post_types( $args, $output );
        
        if( $custom_post_types ) {
            
            foreach( $custom_post_types as $key => $value ) {
                
                $post_types[$key] = $value->labels->name;
            }
        }
        
        return $post_types;
    } 
    
    public function ilsh_checked_post_type( $post_type = null, $data = false ) {
        
        if( ! $post_type ) {
            
            return;
        }
        
        $option_name = 'ilsh_' . $post_type;        
        
        $option = get_option( $option_name );
        
        if( $option && $option == $post_type ) {
            
            return 'checked="checked"';
            
        } else {
            
            return false;
        }
    }
    
    public function ilsh_get_checked_post_types() {
        
        $post_types = $this->islh_get_post_types();
        
        $checked_post_types = array();
        
        foreach( $post_types as $key => $value ) {
            
            if( $this->ilsh_checked_post_type( $key ) ) {
                
                $checked_post_types[] = $key;
            }
        }
        
        return $checked_post_types;
    }    
    
}

