<?php

class ilsh_settings_page extends ilsh_gallery {
        
    public function __construct() {
        
        parent::__construct();
        
        /*
         * create pludin admin settings page
         */
        
        add_action( 'admin_menu', array( $this, 'ilsh_register_menu_page' ) );
    }
    
    public function ilsh_register_menu_page() {
        
        add_menu_page( 
                 $this->setting_page['page-title'],
                 $this->setting_page['menu-title'],
                 'manage_options',
                 $this->setting_page['menu-slug'],
                 array( $this, 'ilsh_menu_page' ), 
                 'dashicons-format-gallery' ,22                
        );
    }
    
    public function ilsh_menu_page() {
         
        $post_types = $this->islh_get_post_types();
        
        //save action 
        $this->ilsh_save_settings( $post_types );
        
        ?>
        
        <h1><?php echo $this->setting_page['page-title'];?></h1>
        
        <h3><?php echo $this->setting_page['page-top-info'];?></h3>
        
        <div class="setting-info-div">
            <?php echo $this->setting_page['page-setting-info'];?>
        </div>
        
        <form method='POST' action=''>
            
            <input type="hidden" name="ilsh_action" value="settings">
            
            <table class="form-table">
                <tbody>
                    
                    <tr>
                        <th scope="row"><?php echo $this->setting_page['fancy-text'];?></th>
                        <td> 
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php echo $this->setting_page['fancy-text'];?></span>
                                </legend>
                                <label>
                                    <input <?php echo get_option( 'ilsh_enabled_fancybox' ) ? 'checked="checked"' : '';?> name="ilsh_enabled_fancybox" type="checkbox" value="1">                                    
                                </label>
                            </fieldset>
                        </td>
                    </tr>
                    
                    <?php foreach( $post_types as $key => $value ):?>
                    
                        <tr>
                            <th scope="row"><?php echo $value;?></th>
                            <td> 
                                <fieldset>
                                    <legend class="screen-reader-text">
                                        <span><?php echo $value;?></span>
                                    </legend>
                                    <label>
                                        <input <?php echo $this->ilsh_checked_post_type( $key );?> name="ilsh_gallery_posts[]" type="checkbox" value="<?php echo $key;?>">                                    
                                    </label>
                                </fieldset>
                            </td>
                        </tr>
                        
                    <?php endforeach;?>
                    
                    <tr>
                        <td>
                            <input type='submit' name='ilsh_settings' class='button button-primary' value='<?php echo $this->setting_page['button-text'];?>'>
                        </td>
                    </tr>
                    
                </tbody>
                
            </table>
            
        </form>
        
        <?php
    }
    
    public function ilsh_save_settings( $post_types ) {
        
        if( isset( $_POST['ilsh_action'] ) && $_POST['ilsh_action'] == 'settings' ) {
            
            //save checked post types 
            $this->ilsh_save_checked_post_types( $post_types );
        }
    }
    
    public function ilsh_save_checked_post_types( $post_types ) {
       
        if( isset( $_POST['ilsh_enabled_fancybox'] ) ) {
            
            update_option( 'ilsh_enabled_fancybox', $_POST['ilsh_gallery_posts'] );
            
        } else {
            
            delete_option( 'ilsh_enabled_fancybox' );
        }
        
        if( isset( $_POST['ilsh_gallery_posts'] ) ) {
                
                foreach( $post_types as $key => $value ) {
                    
                    
                    $option_name = 'ilsh_' . $key;
                    
                    if( in_array( $key, $_POST['ilsh_gallery_posts'] ) ) {
                        
                        update_option( $option_name, $key );
                        
                    } else {
                        
                        delete_option( $option_name );
                    }                    
                }
                
        } else {

            //if all checkboxes - uncheck - delete all old options
            $this->ilsh_delete_old_options( $post_types );
        }
    }
    
    public function ilsh_delete_old_options( $post_types ) {
        
        foreach( $post_types as $key => $value ) {
            
            $option_name = 'ilsh_' . $key;
           
            delete_option( $option_name );
        }        
    }
   
}

new ilsh_settings_page();