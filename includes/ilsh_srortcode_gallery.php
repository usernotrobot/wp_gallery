<?php

function ilsh_show_gallery( $atts ) {
    
    $post_id = null;
    
    $atts = shortcode_atts(
                
		array(
                    
                    'post_id'       => get_the_ID(),
                    'thumb_size'    => null,
                    
		), $atts, 'ilsh_show_gallery' );
    
    $post_id = $atts['post_id'];
    
    if( $post_id ) {
        
        include_once( ILSH_PLUGIN_DIR . '/includes/ilsh_create_gallery.php' ); 
    
        $ilsh_gallery = new ilsh_create_gallery();
        
        $post = get_post( $post_id );
            
        ob_start();

        $ilsh_gallery->ilsh_get_gallery( $post, $atts['thumb_size'] );

        return ob_get_clean();
        
        
    }
}

add_shortcode( 'ilsh_show_gallery', 'ilsh_show_gallery' );
