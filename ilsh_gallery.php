<?php
/*
Plugin Name: Ajax Gallery in selected post-types  
Description: Creating a gallery in any post, any post type, with an option to preview loading images, to load AJAX images one file at a time and load several images at once, sort galleries, display one or several galleries in any convinient place, with the option to switch on/off Fancybox slider to display the gallery.
Version: 1.0.0
Author: Ilya Shaptala
Text Domain: ilsh
*/      

define( 'ILSH_VERSION', '1.0.0' );

define( 'ILSH_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . dirname( plugin_basename( __FILE__ ) ) );

define( 'ILSH_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

include_once( ILSH_PLUGIN_DIR . '/config/ilsh_config.php' );

include_once( ILSH_PLUGIN_DIR . '/includes/ilsh_functions.php' );

class ilsh_gallery extends ilsh_functions {
    
    public function __construct() {
        
        parent::__construct();
        
        $this->ilsh_includes();
        
        add_action( 'wp_enqueue_scripts', array( $this, 'ilsh_enqueue_scripts_styles' ) );
        
        add_action( 'admin_enqueue_scripts', array( $this, 'ilsh_enqueue_scripts_styles' ) );  
    }
    
    public function ilsh_includes() {
         
        include_once( ILSH_PLUGIN_DIR . '/includes/ilsh_settings_page.php' );
        
        include_once( ILSH_PLUGIN_DIR . '/includes/ilsh_create_gallery.php' );
        
        include_once( ILSH_PLUGIN_DIR . '/includes/ilsh_srortcode_gallery.php' );        
    }
    
    public function ilsh_enqueue_scripts_styles() {
        
        wp_enqueue_style( 'style', plugins_url( '/assets/css/style.css' , __FILE__ ), false, ILSH_VERSION, false );
        
        if( is_admin() ) { 
            
            wp_enqueue_style("wp-jquery-ui-dialog");
             
            wp_enqueue_script(
                array(
			'jquery',
			'jquery-ui-core',
			'jquery-ui-tabs',
			'jquery-ui-sortable',
                        'jquery-ui-dialog'
		));     
            
            add_thickbox();
            
            wp_register_script( 'ilsh_admin',  plugins_url( '/assets/js/ilsh_admin.js' , __FILE__ ) );
                        
            wp_localize_script( 'ilsh_admin', '_admin_data',
                    
                array( 
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
                    'ilsh_js'  => $this->ilsh_js
                ) 
            );
             
            wp_enqueue_script( 'ilsh_admin' );
            
        } else {            
            
            //fancybox
            if( $this->enabled_fancybox ) {
            
                wp_enqueue_style( 'mr_fancybox_css', plugins_url( '/assets/fancybox/source/jquery.fancybox.css' , __FILE__ ), false, '2.1.5', false );

                wp_enqueue_style( 'mr_fancybox_buttons_css', plugins_url( '/assets/fancybox/source/helpers/jquery.fancybox-buttons.css' , __FILE__ ), false, '1.0.5', false );

                wp_enqueue_style( 'mr_fancybox_thumbs_css', plugins_url( '/assets/fancybox/source/helpers/jquery.fancybox-thumbs.css' , __FILE__ ), false, '1.0.7', false );
            }
            
            
            wp_enqueue_script( array( 'jquery' ) );            
            
            //fancybox
            if( $this->enabled_fancybox ) {
                
                wp_register_script( 'mr_mousewheel',  plugins_url( '/assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js' , __FILE__ ) );

                wp_enqueue_script( 'mr_mousewheel' );

                wp_register_script( 'mr_fancybox',  plugins_url( '/assets/fancybox/source/jquery.fancybox.pack.js' , __FILE__ ) );

                wp_enqueue_script( 'mr_fancybox' );

                wp_register_script( 'mr_fancybox_buttons',  plugins_url( '/assets/fancybox/source/helpers/jquery.fancybox-buttons.js' , __FILE__ ) );

                wp_enqueue_script( 'mr_fancybox_buttons' );

                wp_register_script( 'mr_fancybox_media',  plugins_url( '/assets/fancybox/source/helpers/jquery.fancybox-media.js' , __FILE__ ) );

                wp_enqueue_script( 'mr_fancybox_media' );

                wp_register_script( 'mr_fancybox_thumbs',  plugins_url( '/assets/fancybox/source/helpers/jquery.fancybox-thumbs.js' , __FILE__ ) );

                wp_enqueue_script( 'mr_fancybox_thumbs' );
            }
            
            wp_register_script( 'ilsh_front',  plugins_url( '/assets/js/ilsh_front.js' , __FILE__ ) );
                        
            wp_localize_script( 'ilsh_front', '_admin_data',
                    
                array( 
                    'ajax_url' => admin_url( 'admin-ajax.php' )                   
                ) 
            );
             
            wp_enqueue_script( 'ilsh_front' );
        }
    }
    
}


new ilsh_gallery();